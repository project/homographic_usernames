<?php
/**
 * Unicode normalization routines
 *
 * Copyright Â© 2004 Brion Vibber <brion@pobox.com>
 * http://www.mediawiki.org/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup UtfNormal
 */

/**
 * @defgroup UtfNormal UtfNormal
 */

define( 'UNICODE_HANGUL_FIRST', 0xac00 );
define( 'UNICODE_HANGUL_LAST',  0xd7a3 );

define( 'UNICODE_HANGUL_LBASE', 0x1100 );
define( 'UNICODE_HANGUL_VBASE', 0x1161 );
define( 'UNICODE_HANGUL_TBASE', 0x11a7 );

define( 'UNICODE_HANGUL_LCOUNT', 19 );
define( 'UNICODE_HANGUL_VCOUNT', 21 );
define( 'UNICODE_HANGUL_TCOUNT', 28 );
define( 'UNICODE_HANGUL_NCOUNT', UNICODE_HANGUL_VCOUNT * UNICODE_HANGUL_TCOUNT );

define( 'UNICODE_HANGUL_LEND', UNICODE_HANGUL_LBASE + UNICODE_HANGUL_LCOUNT - 1 );
define( 'UNICODE_HANGUL_VEND', UNICODE_HANGUL_VBASE + UNICODE_HANGUL_VCOUNT - 1 );
define( 'UNICODE_HANGUL_TEND', UNICODE_HANGUL_TBASE + UNICODE_HANGUL_TCOUNT - 1 );

define( 'UNICODE_SURROGATE_FIRST', 0xd800 );
define( 'UNICODE_SURROGATE_LAST', 0xdfff );
define( 'UNICODE_MAX', 0x10ffff );
define( 'UNICODE_REPLACEMENT', 0xfffd );


define( 'UTF8_HANGUL_FIRST', "\xea\xb0\x80" /*codepointToUtf8( UNICODE_HANGUL_FIRST )*/ );
define( 'UTF8_HANGUL_LAST', "\xed\x9e\xa3" /*codepointToUtf8( UNICODE_HANGUL_LAST )*/ );

define( 'UTF8_HANGUL_LBASE', "\xe1\x84\x80" /*codepointToUtf8( UNICODE_HANGUL_LBASE )*/ );
define( 'UTF8_HANGUL_VBASE', "\xe1\x85\xa1" /*codepointToUtf8( UNICODE_HANGUL_VBASE )*/ );
define( 'UTF8_HANGUL_TBASE', "\xe1\x86\xa7" /*codepointToUtf8( UNICODE_HANGUL_TBASE )*/ );

define( 'UTF8_HANGUL_LEND', "\xe1\x84\x92" /*codepointToUtf8( UNICODE_HANGUL_LEND )*/ );
define( 'UTF8_HANGUL_VEND', "\xe1\x85\xb5" /*codepointToUtf8( UNICODE_HANGUL_VEND )*/ );
define( 'UTF8_HANGUL_TEND', "\xe1\x87\x82" /*codepointToUtf8( UNICODE_HANGUL_TEND )*/ );

define( 'UTF8_SURROGATE_FIRST', "\xed\xa0\x80" /*codepointToUtf8( UNICODE_SURROGATE_FIRST )*/ );
define( 'UTF8_SURROGATE_LAST', "\xed\xbf\xbf" /*codepointToUtf8( UNICODE_SURROGATE_LAST )*/ );
define( 'UTF8_MAX', "\xf4\x8f\xbf\xbf" /*codepointToUtf8( UNICODE_MAX )*/ );
define( 'UTF8_REPLACEMENT', "\xef\xbf\xbd" /*codepointToUtf8( UNICODE_REPLACEMENT )*/ );
#define( 'UTF8_REPLACEMENT', '!' );

define( 'UTF8_OVERLONG_A', "\xc1\xbf" );
define( 'UTF8_OVERLONG_B', "\xe0\x9f\xbf" );
define( 'UTF8_OVERLONG_C', "\xf0\x8f\xbf\xbf" );

# These two ranges are illegal
define( 'UTF8_FDD0', "\xef\xb7\x90" /*codepointToUtf8( 0xfdd0 )*/ );
define( 'UTF8_FDEF', "\xef\xb7\xaf" /*codepointToUtf8( 0xfdef )*/ );
define( 'UTF8_FFFE', "\xef\xbf\xbe" /*codepointToUtf8( 0xfffe )*/ );
define( 'UTF8_FFFF', "\xef\xbf\xbf" /*codepointToUtf8( 0xffff )*/ );

define( 'UTF8_HEAD', false );
define( 'UTF8_TAIL', true );

/**
 * Unicode normalization routines for working with UTF-8 strings.
 * Currently assumes that input strings are valid UTF-8!
 *
 * Not as fast as I'd like, but should be usable for most purposes.
 * UtfNormal::toNFC() will bail early if given ASCII text or text
 * it can quickly deterimine is already normalized.
 *
 * All functions can be called static.
 *
 * See description of forms at http://www.unicode.org/reports/tr15/
 *
 * @ingroup UtfNormal
 */
class UtfNormal {
  static $utfCombiningClass = null;
  static $utfCanonicalComp = null;
  static $utfCanonicalDecomp = null;

  static $utfCheckNFC;

  /**
   * Convert a UTF-8 string to normal form D, canonical decomposition.
   * Fast return for pure ASCII strings.
   *
   * @param $string String: a valid UTF-8 string. Input is not validated.
   * @return string a UTF-8 string in normal form D
   */
  static function toNFD( $string ) {
    if( function_exists( 'normalizer_normalize' ) )
      return normalizer_normalize( $string, Normalizer::FORM_D );
    elseif( function_exists( 'utf8_normalize' ) )
      return utf8_normalize( $string, UNORM_NFD );
    elseif( preg_match( '/[\x80-\xff]/', $string ) )
      return UtfNormal::NFD( $string );
    else
      return $string;
  }

  /**
   * @param $string string
   * @return string
   * @private
   */
  static function NFD( $string ) {
    UtfNormal::loadData();

    return UtfNormal::fastCombiningSort(
      UtfNormal::fastDecompose( $string, self::$utfCanonicalDecomp ) );
  }

  /**
   * Load the basic composition data if necessary
   * @private
   */
  static function loadData() {
    if( !isset( self::$utfCombiningClass ) ) {
      require_once( dirname(__FILE__) . '/homographic_usernames.tr15.inc' );
    }
  }


  /**
   * Perform decomposition of a UTF-8 string into either D or KD form
   * (depending on which decomposition map is passed to us).
   * Input is assumed to be *valid* UTF-8. Invalid code will break.
   * @private
   * @param $string String: valid UTF-8 string
   * @param $map Array: hash of expanded decomposition map
   * @return string a UTF-8 string decomposed, not yet normalized (needs sorting)
   */
  static function fastDecompose( $string, $map ) {
    $len = strlen( $string );
    $out = '';
    for( $i = 0; $i < $len; $i++ ) {
      $c = $string{$i};
      $n = ord( $c );
      if( $n < 0x80 ) {
        # ASCII chars never decompose
        # THEY ARE IMMORTAL
        $out .= $c;
        continue;
      } elseif( $n >= 0xf0 ) {
        $c = substr( $string, $i, 4 );
        $i += 3;
      } elseif( $n >= 0xe0 ) {
        $c = substr( $string, $i, 3 );
        $i += 2;
      } elseif( $n >= 0xc0 ) {
        $c = substr( $string, $i, 2 );
        $i++;
      }
      if( isset( $map[$c] ) ) {
        $out .= $map[$c];
        continue;
      } else {
        if( $c >= UTF8_HANGUL_FIRST && $c <= UTF8_HANGUL_LAST ) {
          # Decompose a hangul syllable into jamo;
          # hardcoded for three-byte UTF-8 sequence.
          # A lookup table would be slightly faster,
          # but adds a lot of memory & disk needs.
          #
          $index = ( (ord( $c{0} ) & 0x0f) << 12
                   | (ord( $c{1} ) & 0x3f) <<  6
                   | (ord( $c{2} ) & 0x3f) )
                 - UNICODE_HANGUL_FIRST;
          $l = intval( $index / UNICODE_HANGUL_NCOUNT );
          $v = intval( ($index % UNICODE_HANGUL_NCOUNT) / UNICODE_HANGUL_TCOUNT);
          $t = $index % UNICODE_HANGUL_TCOUNT;
          $out .= "\xe1\x84" . chr( 0x80 + $l ) . "\xe1\x85" . chr( 0xa1 + $v );
          if( $t >= 25 ) {
            $out .= "\xe1\x87" . chr( 0x80 + $t - 25 );
          } elseif( $t ) {
            $out .= "\xe1\x86" . chr( 0xa7 + $t );
          }
          continue;
        }
      }
      $out .= $c;
    }
    return $out;
  }

  /**
   * Sorts combining characters into canonical order. This is the
   * final step in creating decomposed normal forms D and KD.
   * @private
   * @param $string String: a valid, decomposed UTF-8 string. Input is not validated.
   * @return string a UTF-8 string with combining characters sorted in canonical order
   */
  static function fastCombiningSort( $string ) {
    $len = strlen( $string );
    $out = '';
    $combiners = array();
    $lastClass = -1;
    for( $i = 0; $i < $len; $i++ ) {
      $c = $string{$i};
      $n = ord( $c );
      if( $n >= 0x80 ) {
        if( $n >= 0xf0 ) {
          $c = substr( $string, $i, 4 );
          $i += 3;
        } elseif( $n >= 0xe0 ) {
          $c = substr( $string, $i, 3 );
          $i += 2;
        } elseif( $n >= 0xc0 ) {
          $c = substr( $string, $i, 2 );
          $i++;
        }
        if( isset( self::$utfCombiningClass[$c] ) ) {
          $lastClass = self::$utfCombiningClass[$c];
          if( isset( $combiners[$lastClass] ) ) {
            $combiners[$lastClass] .= $c;
          } else {
            $combiners[$lastClass] = $c;
          }
          continue;
        }
      }
      if( $lastClass ) {
        ksort( $combiners );
        $out .= implode( '', $combiners );
        $combiners = array();
      }
      $out .= $c;
      $lastClass = 0;
    }
    if( $lastClass ) {
      ksort( $combiners );
      $out .= implode( '', $combiners );
    }
    return $out;
  }

}