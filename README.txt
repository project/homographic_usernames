
The Homographic Usernames modules prevents people from creating user accounts
with homographic usernames. That is, they may not create usernames that *look*
the same, but use letters from different alphabets. This prevents impersonation
attacks.

Upon installation, the homographic usernames module saves a version of the
username with ambiguous letters converted into their canonical equivalents.
Users are allowed to keep their existing usernames (even if they conflict),
but upon trying to change their username, they are forced to pick a username
which doesn't have any homographic duplicates.