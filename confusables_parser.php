<?php

$associations = array();
$reverse_map = array();
$id = 0;

$handle = fopen("confusables.txt", "r");
if ($handle) {
  while (($buffer = fgets($handle, 4096)) !== FALSE) {
    list($non_comment) = explode('#', $buffer);
    if (trim($non_comment)) {
      list($from, $to) = explode(';', $non_comment);
      // These cases are handled by user_validate_name, so no need to include
      // them in the list of confusables
      if (!empty($from) && !empty($to) && !preg_match('/[^\x{80}-\x{F7} a-z0-9@_.\'-]/i', $from_string)
        && !preg_match('/[\x{80}-\x{A0}' .         // Non-printable ISO-8859-1 + NBSP
                      '\x{AD}' .                // Soft-hyphen
                      '\x{2000}-\x{200F}' .     // Various space characters
                      '\x{2028}-\x{202F}' .     // Bidirectional text overrides
                      '\x{205F}-\x{206F}' .     // Various text hinting characters
                      '\x{FEFF}' .              // Byte order mark
                      '\x{FF01}-\x{FF60}' .     // Full-width latin
                      '\x{FFF9}-\x{FFFD}' .     // Replacement characters
                      '\x{0}-\x{1F}]/u',         // NULL byte and control characters
                      $from_string)) {
        $from_utf8 = unicodeToUtf8(explode(' ', trim($from)));
        $to_utf8 = unicodeToUtf8(explode(' ', trim($to)));
        if (isset($associations[$from_utf8]) && isset($associations[$to_utf8]) && $associations[$from_utf8] != $associations[$to_utf8]) {
          // Merge 2 groups, using id of the to.
          $from_group = $reverse_map[$associations[$from_utf8]];
          unset($reverse_map[$associations[$from_utf8]]);
          $reverse_map[$associations[$to_utf8]] = array_merge($reverse_map[$associations[$to_utf8]], $from_group);
          foreach ($from_group as $letter) {
            $associations[$letter] = $associations[$to_utf8];
          }
        }
        elseif (isset($assocations[$from_utf8])) {
          $associations[$to_utf8] = $associations[$from_utf8];
          $reverse_map[$associations[$from_utf8]][] = $to_utf8;
        }
        elseif (isset($associations[$to_utf8])) {
          $associations[$from_utf8] = $associations[$to_utf8];
          $reverse_map[$associations[$to_utf8]][] = $from_utf8;
        }
        else {
          $id++;
          $associations[$from_utf8] = $associations[$to_utf8] = $id;
          $reverse_map[$id] = array($from_utf8, $to_utf8);
        }

      }
    }
  }
  fclose($handle);
}

$write_handle = fopen('homographic_usernames.confusables.inc', 'w');
fwrite($write_handle, <<<EOF
<?php
// \$Id\$

/**
 * Returns an associative array of confusing characters and their canonical
 * equivalents.
 * 
 * This file was automatically generated from
 * http://unicode.org/Public/security/revision-03/confusables.txt, which is
 * mirrored in confusables.txt.
 */
function homographic_usernames_confusables() {
 return array(

EOF
);

foreach ($associations as $k => $id) {
  fwrite($write_handle, '    ' . var_export($k, 1) . ' => ' . $id . ",\n");
}

fwrite($write_handle, "  );\n}\n");
fclose($write_handle);

//----------------------------------------------------------------

/**
 * The Original Code is Mozilla Communicator client code.
 *
 * The Initial Developer of the Original Code is
 * Netscape Communications Corporation.
 * Portions created by the Initial Developer are Copyright (C) 1998
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 * Henri Sivonen, hsivonen@iki.fi
 *
 */

/*
 * For the original C++ code, see
 * http://lxr.mozilla.org/seamonkey/source/intl/uconv/src/nsUTF8ToUnicode.cpp
 * http://lxr.mozilla.org/seamonkey/source/intl/uconv/src/nsUnicodeToUTF8.cpp
 *
 * The latest version of this file can be obtained from
 * http://iki.fi/hsivonen/php-utf8/
 *
 * Version 1.0, 2003-05-30
 */

/**
 * Takes an array of ints representing the Unicode characters and returns 
 * a UTF-8 string. Astral planes are supported ie. the ints in the
 * input can be > 0xFFFF. Occurrances of the BOM are ignored. Surrogates
 * are not allowed.
 *
 * Returns false if the input array contains ints that represent 
 * surrogates or are outside the Unicode range.
 */
function unicodeToUtf8($arr) {
  $dest = '';
  foreach ($arr as $src) {
    $src = hexdec(trim($src));
    if (!$src) {
      continue;
    }
    if($src < 0) {
      return false;
    } else if ( $src <= 0x007f) {
      $dest .= chr($src);
    } else if ($src <= 0x07ff) {
      $dest .= chr(0xc0 | ($src >> 6));
      $dest .= chr(0x80 | ($src & 0x003f));
    } else if($src == 0xFEFF) {
      // nop -- zap the BOM
    } else if ($src >= 0xD800 && $src <= 0xDFFF) {
      // found a surrogate
      return false;
    } else if ($src <= 0xffff) {
      $dest .= chr(0xe0 | ($src >> 12));
      $dest .= chr(0x80 | (($src >> 6) & 0x003f));
      $dest .= chr(0x80 | ($src & 0x003f));
    } else if ($src <= 0x10ffff) {
      $dest .= chr(0xf0 | ($src >> 18));
      $dest .= chr(0x80 | (($src >> 12) & 0x3f));
      $dest .= chr(0x80 | (($src >> 6) & 0x3f));
      $dest .= chr(0x80 | ($src & 0x3f));
    } else { 
      // out of range
      return false;
    }
  }
  return $dest;
}
